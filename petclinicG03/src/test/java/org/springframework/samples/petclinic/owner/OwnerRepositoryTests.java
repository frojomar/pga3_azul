package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class OwnerRepositoryTests {

	@Autowired
	private OwnerRepository ownerRepository;
	
	private Owner ownerJuan;
	private Owner ownerPepe;
	private Owner ownerLaura;
	
	@Before
	public void init() {
		//Collection <Vet> cVets = 	
		
		PetType petType = new PetType();
		petType.setId(1);
		petType.setName("Dog");
		
		if (this.ownerJuan==null) {
			ownerJuan = new Owner();
			ownerJuan.setFirstName("Juan");
			ownerJuan.setLastName("Perez");
			ownerJuan.setAddress("Plaza de España");
			ownerJuan.setCity("Caceres");
			ownerJuan.setTelephone("111111111");
			
			Pet petCopo = new Pet();
			petCopo.setBirthDate(LocalDate.parse("2014-04-03"));
			petCopo.setWeight((float)10.6);
			petCopo.setComments("It sings");
			petCopo.setType(petType);
			petCopo.setOwner(ownerJuan);

			
			ownerJuan.addPet(petCopo);
			
			this.ownerRepository.save(ownerJuan);
			System.out.println("id de juan" + ownerJuan.getId());
		}
		
		if (this.ownerPepe==null) {
			ownerPepe = new Owner();
			ownerPepe.setFirstName("Pepe");
			ownerPepe.setLastName("Lopez");
			ownerPepe.setAddress("Plaza de America");
			ownerPepe.setCity("Caceres");
			ownerPepe.setTelephone("222222222");
			
			Pet petJacky = new Pet();
			petJacky.setBirthDate(LocalDate.parse("2008-05-10"));
			petJacky.setWeight((float)10.6);
			petJacky.setComments("It bites");
			petJacky.setType(petType);
			petJacky.setOwner(ownerPepe);
			
			ownerPepe.addPet(petJacky);
			
			this.ownerRepository.save(ownerPepe);
		}

		if (this.ownerLaura==null) {
			ownerLaura = new Owner();
			ownerLaura.setFirstName("Laura");
			ownerLaura.setLastName("Rojo");
			ownerLaura.setAddress("Moctezuma");
			ownerLaura.setCity("Caceres");
			ownerLaura.setTelephone("333333333");
			
			Pet petFlo = new Pet();
			petFlo.setBirthDate(LocalDate.parse("2012-07-26"));
			petFlo.setWeight((float)4.2);
			petFlo.setComments("It says meow");
			petFlo.setType(petType);
			petFlo.setOwner(ownerLaura);
			
			ownerLaura.addPet(petFlo);
			
			this.ownerRepository.save(ownerLaura);
		}
		
	}
	
	@Test
	@Transactional
	public void testDeleteById() {
		
		//Borramos al owner Juan
		this.ownerRepository.save(ownerJuan);
		
		this.ownerRepository.deleteById(ownerJuan.getId());
		
		//Buscamos el owner Juan
		Owner newOwner = this.ownerRepository.findById(ownerJuan.getId());
		assertNull(newOwner);
		
		//Comprobamos que Laura y Pepe siguen en la BD
		Owner newOwner2 = this.ownerRepository.findById(ownerLaura.getId());
		assertNotNull(newOwner2);
		
		Owner newOwner3 = this.ownerRepository.findById(ownerPepe.getId());
		assertNotNull(newOwner3);
	}
	
	@After
	public void finish() {
		this.ownerJuan=null;
		this.ownerPepe=null;
		this.ownerLaura=null;
	}
	
}
