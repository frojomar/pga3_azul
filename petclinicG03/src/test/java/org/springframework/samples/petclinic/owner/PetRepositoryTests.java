package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class PetRepositoryTests {
	
	@Autowired
	private PetRepository pets;
	
	private Pet pet;
	
	@Before
	public void init() {
		//Collection <Pet> cPets = 
		
		if (this.pet==null) {
			pet = new Pet ();
			
			Owner ownerJuan = new Owner();
			ownerJuan.setId(1);
			ownerJuan.setFirstName("Juan");
			ownerJuan.setLastName("Perez");
			ownerJuan.setAddress("Plaza de España");
			ownerJuan.setCity("Caceres");
			ownerJuan.setTelephone("111111111");
			
			PetType type= new PetType();
			type.setId(1);
			type.setName("dog");
			
			
			pet.setBirthDate(LocalDate.parse("2008-05-10"));
			pet.setWeight((float)10.6);
			pet.setComments("It bites");
			pet.setName("Jack");
			pet.setType(type);
			pet.setOwner(ownerJuan);

			ownerJuan.addPet(pet);

			this.pets.save(pet);
		}
	}
	
	
	// NR10
	
	@Test
	public void testFindByName() {
		Collection<Pet> petFindByName = this.pets.findPetsByName(pet.getName());
		
		assertNotNull(petFindByName);
		assertEquals(petFindByName.size(),1);
		
		for(Pet aPet:petFindByName) {
			assertEquals(aPet.getName(), pet.getName());
			assertEquals(aPet.getBirthDate(), pet.getBirthDate());
			assertEquals(aPet.getId(), pet.getId());
			assertEquals(aPet.getComments(), pet.getComments());
			assertEquals(aPet.getOwner(), pet.getOwner());
			assertEquals(aPet.getType(), pet.getType());
			assertEquals(aPet.getWeight(), pet.getWeight(), 0);
			if (aPet.getVisits().size()>0) {
				assertArrayEquals(aPet.getVisits().toArray(), pet.getVisits().toArray());
			}
		}
	}
	
	//FIN NR10
	
	@Test
	public void testDeletePet() {
		//Borramos a la pet pet
		this.pets.save(pet);

		assertNotNull(pet.getVisits());

		this.pets.deletePetInfo(pet.getId());
		this.pets.delete(pet);

		//Buscamos a la pet Pet
		Pet newPet = this.pets.findById(pet.getId());
		assertNull(newPet);
	}
	
	@After
	public void finish() {
		this.pet=null;
	}
	

}