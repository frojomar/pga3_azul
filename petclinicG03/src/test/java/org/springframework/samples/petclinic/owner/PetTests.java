package org.springframework.samples.petclinic.owner;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import org.springframework.samples.petclinic.visit.Visit;

public class PetTests {

	public static Owner ownerPepe;
	
	public static Pet petJacky;
	
	@Before
	public void init() {
		PetType petType = new PetType();
		petType.setId(1);
		petType.setName("Dog");

		petJacky = new Pet();
		petJacky.setBirthDate(LocalDate.parse("2008-05-10"));
		petJacky.setWeight((float)10.6);
		petJacky.setComments("It bites");
		petJacky.setType(petType);
		petJacky.setOwner(ownerPepe);
		
		ownerPepe = new Owner();
		ownerPepe.setFirstName("Pepe");
		ownerPepe.setLastName("Lopez");
		ownerPepe.setAddress("Plaza de America");
		ownerPepe.setCity("Caceres");
		ownerPepe.setTelephone("222222222");
		ownerPepe.addPet(petJacky);
	}
	
	@Test
	public void testWeight() {
		assertNotEquals(petJacky.getWeight(), (float)23.4);
		assertEquals(petJacky.getWeight(), (float)10.6, 0);
		
		petJacky.setWeight((float)15.4);
		
		assertNotEquals(petJacky.getWeight(), (float)23.4);
		assertEquals(petJacky.getWeight(), (float)15.4, 0);
	}
	
	@Test
	public void testComments() {
		assertNotNull(petJacky.getComments());
		assertNotEquals(petJacky.getComments(), "It hurts");
		assertEquals(petJacky.getComments(), "It bites");
		
		petJacky.setComments("No comments");
		
		assertNotNull(petJacky.getComments());
		assertNotEquals(petJacky.getComments(), "It hurts");
		assertEquals(petJacky.getComments(), "No comments");
	}
	
	@Test
	public void testVisits() {
		assertEquals(petJacky.getVisits().size(), 0);

		Visit visit1= new Visit();
		visit1.setDate(LocalDate.parse("2010-05-26"));
		visit1.setDescription("Mascota con pata rota");
		visit1.setPetId(1);
		visit1.setVet(null);

		petJacky.addVisit(visit1);

		assertNotNull(petJacky.getVisits());
		assertEquals(petJacky.getVisits().size(), 1);

		Visit visit2= new Visit();
		visit2.setDate(LocalDate.parse("2013-03-24"));
		visit2.setDescription("Mascota con ala rota");
		visit2.setPetId(2);
		visit2.setVet(null);

		petJacky.addVisit(visit2);

		assertNotNull(petJacky.getVisits());
		assertEquals(petJacky.getVisits().size(), 2);

	}
	

}
