package org.springframework.samples.petclinic.owner;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.time.LocalDate;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.PetRepository;
import org.springframework.samples.petclinic.owner.VisitController;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.samples.petclinic.visit.VisitRepository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Test class for {@link VisitController}
 *
 * @author Colin But
 */
@RunWith(SpringRunner.class)
@WebMvcTest(VisitController.class)
public class VisitControllerTests {

    private static final int TEST_PET_ID = 1;
    private static final int TEST_VISIT_ID = 1;
    private static final int TEST_OWNER_ID = 1;


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VisitRepository visits;

    private Visit visit;

    @Before
    public void init() {
    	
        PetType cat = new PetType();
        cat.setId(3);
        cat.setName("hamster");
        
        Pet lucky= new Pet();
        lucky.setId(1);
        lucky.setName("Lucky");
        lucky.setType(cat);
        
		Owner ownerJuan = new Owner();
		ownerJuan.setId(1);
		ownerJuan.setFirstName("Juan");
		ownerJuan.setLastName("Perez");
		ownerJuan.setAddress("Plaza de España");
		ownerJuan.setCity("Caceres");
		ownerJuan.setTelephone("111111111");
		
		lucky.setOwner(ownerJuan);

		ownerJuan.addPet(lucky);
		
		Vet vet = new Vet();
		
		vet.setFirstName("Jose");
		vet.setHomeVisits(false);
		vet.setId(1);
		vet.setLastName("Rojo");
		
		visit.setVet(vet);
		visit.setDate(LocalDate.parse("2018-12-12"));
		visit.setDescription("Ala rota");
		visit.setPetId(lucky.getId());
		visit.setId(1);

		
        given(this.visits.findById(TEST_VISIT_ID)).willReturn(visit);
    }

    @Test
    public void testInitNewVisitForm() throws Exception {
        mockMvc.perform(get("/owners/*/pets/{petId}/visits/new", TEST_PET_ID))
            .andExpect(status().isOk())
            .andExpect(view().name("pets/createOrUpdateVisitForm"));
    }

    @Test
    public void testProcessNewVisitFormSuccess() throws Exception {
        mockMvc.perform(post("/owners/*/pets/{petId}/visits/new", TEST_PET_ID)
            .param("name", "George")
            .param("description", "Visit Description")
        )
            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/owners/{ownerId}"));
    }

    @Test
    public void testProcessNewVisitFormHasErrors() throws Exception {
        mockMvc.perform(post("/owners/*/pets/{petId}/visits/new", TEST_PET_ID)
            .param("name", "George")
        )
            .andExpect(model().attributeHasErrors("visit"))
            .andExpect(status().isOk())
            .andExpect(view().name("pets/createOrUpdateVisitForm"));
    }
    
    // NR6
    
    @Test
    public void testInitEditForm() throws Exception {
        mockMvc.perform(get("/owners/*/pets/{petId}/visits/{visitId}/edit", TEST_PET_ID, TEST_VISIT_ID))
	        .andExpect(status().isOk())
	        .andExpect(model().attributeExists("visit"))
	        .andExpect(view().name("pets/createOrUpdateVisitForm"));
    }
    //FIN NR6

}
