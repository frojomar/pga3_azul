package org.springframework.samples.petclinic.selenium;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class EditVisitDetail {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testEditVisitDetail() throws Exception {
    driver.get("http://localhost:8080/owners/14");
    driver.findElement(By.linkText("Veterinarians")).click();
    driver.findElement(By.linkText("Add Vet")).click();
    driver.findElement(By.id("firstName")).click();
    driver.findElement(By.id("firstName")).clear();
    driver.findElement(By.id("firstName")).sendKeys("Marcos");
    driver.findElement(By.id("lastName")).clear();
    driver.findElement(By.id("lastName")).sendKeys("Fernandez");
    // ERROR: Caught exception [ERROR: Unsupported command [addSelection | id=specialties | label=dentistry]]
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Specialties'])[1]/following::option[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Specialties'])[1]/following::button[1]")).click();
    driver.findElement(By.linkText("Find owners")).click();
    driver.findElement(By.linkText("Add Owner")).click();
    driver.findElement(By.id("firstName")).click();
    driver.findElement(By.id("firstName")).clear();
    driver.findElement(By.id("firstName")).sendKeys("Laura");
    driver.findElement(By.id("lastName")).clear();
    driver.findElement(By.id("lastName")).sendKeys("Hernandez");
    driver.findElement(By.id("address")).clear();
    driver.findElement(By.id("address")).sendKeys("Gomez Becerra");
    driver.findElement(By.id("city")).clear();
    driver.findElement(By.id("city")).sendKeys("Caceres");
    driver.findElement(By.id("telephone")).clear();
    driver.findElement(By.id("telephone")).sendKeys("666666666");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Telephone'])[1]/following::button[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Pets and Visits'])[1]/preceding::a[1]")).click();
    driver.findElement(By.id("name")).click();
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Luck");
    driver.findElement(By.id("birthDate")).click();
    driver.findElement(By.id("birthDate")).clear();
    driver.findElement(By.id("birthDate")).sendKeys("1999-03-04");
    new Select(driver.findElement(By.id("type"))).selectByVisibleText("hamster");
    driver.findElement(By.id("type")).click();
    driver.findElement(By.id("weight")).click();
    driver.findElement(By.id("weight")).clear();
    driver.findElement(By.id("weight")).sendKeys("5.0");
    driver.findElement(By.id("comments")).click();
    driver.findElement(By.id("comments")).clear();
    driver.findElement(By.id("comments")).sendKeys("Nada");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[1]/following::button[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/following::a[2]")).click();
    driver.findElement(By.id("description")).click();
    driver.findElement(By.id("description")).clear();
    driver.findElement(By.id("description")).sendKeys("Pata rota");
    new Select(driver.findElement(By.id("vetSelect"))).selectByVisibleText("Marcos");
    driver.findElement(By.id("vetSelect")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Vet'])[1]/following::button[1]")).click();
    driver.findElement(By.linkText("Edit")).click();
    try {
      assertEquals("Update Visit", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Vet'])[1]/following::button[1]")).getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    try {
      assertEquals("", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Vet'])[1]/following::button[1]")).getAttribute("value"));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    driver.findElement(By.id("description")).click();
    driver.findElement(By.id("description")).click();
    driver.findElement(By.id("description")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [doubleClick | id=description | ]]
    driver.findElement(By.id("description")).clear();
    driver.findElement(By.id("description")).sendKeys("Ala rota");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Vet'])[1]/following::button[1]")).click();
    assertEquals("Ala rota", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/following::td[2]")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
