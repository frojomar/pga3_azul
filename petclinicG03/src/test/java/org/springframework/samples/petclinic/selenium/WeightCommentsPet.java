package org.springframework.samples.petclinic.selenium;

import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class WeightCommentsPet {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testWeightCommentsPet() throws Exception {
    driver.get("http://localhost:8080/owners/find");
    driver.findElement(By.linkText("Add Owner")).click();
    driver.findElement(By.id("firstName")).clear();
    driver.findElement(By.id("firstName")).sendKeys("Juan");
    driver.findElement(By.id("lastName")).clear();
    driver.findElement(By.id("lastName")).sendKeys("Perez");
    driver.findElement(By.id("address")).clear();
    driver.findElement(By.id("address")).sendKeys("Plaza de España");
    driver.findElement(By.id("city")).clear();
    driver.findElement(By.id("city")).sendKeys("Caceres");
    driver.findElement(By.id("telephone")).clear();
    driver.findElement(By.id("telephone")).sendKeys("111111111");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Telephone'])[1]/following::button[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Pets and Visits'])[1]/preceding::a[1]")).click();
    assertEquals("Weight", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Type'])[1]/following::label[1]")).getText());
    assertEquals("0.0", driver.findElement(By.id("weight")).getAttribute("value"));
    assertEquals("Comments", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Weight'])[1]/following::label[1]")).getText());
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Jacky");
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Flo");
    driver.findElement(By.id("birthDate")).clear();
    driver.findElement(By.id("birthDate")).sendKeys("2012-07-26");
    new Select(driver.findElement(By.id("type"))).selectByVisibleText("cat");
    driver.findElement(By.id("comments")).clear();
    driver.findElement(By.id("comments")).sendKeys("It says meow");
    driver.findElement(By.id("weight")).clear();
    driver.findElement(By.id("weight")).sendKeys("4.2");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[1]/following::button[1]")).click();
    assertEquals("Flo", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Name'])[2]/following::dd[1]")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
