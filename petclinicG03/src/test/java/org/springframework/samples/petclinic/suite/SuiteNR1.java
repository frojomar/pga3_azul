package org.springframework.samples.petclinic.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.samples.petclinic.owner.PetTests;
import org.springframework.samples.petclinic.selenium.FloatInput;
import org.springframework.samples.petclinic.selenium.WeightCommentsPet;

@RunWith(Suite.class)
@SuiteClasses({ PetTests.class, FloatInput.class, WeightCommentsPet.class })
public class SuiteNR1 {

}
