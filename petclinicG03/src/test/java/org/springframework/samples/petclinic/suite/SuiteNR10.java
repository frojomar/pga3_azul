package org.springframework.samples.petclinic.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.data.repository.core.support.PersistenceExceptionTranslationRepositoryProxyPostProcessor;
import org.springframework.samples.petclinic.owner.PetControllerTests;
import org.springframework.samples.petclinic.owner.PetRepositoryTests;
import org.springframework.samples.petclinic.selenium.FindPets;
import org.springframework.samples.petclinic.selenium.FindPetsLayout;
import org.springframework.samples.petclinic.selenium.PetList;

@RunWith(Suite.class)
@SuiteClasses({PetControllerTests.class, PetRepositoryTests.class, FindPets.class, PetList.class,
	FindPetsLayout.class})
public class SuiteNR10 {

}
