package org.springframework.samples.petclinic.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.samples.petclinic.selenium.CheckSettingsButton;
import org.springframework.samples.petclinic.vet.VetControllerTests;
import org.springframework.samples.petclinic.vet.VetRepositoryTest;

@RunWith(Suite.class)
@SuiteClasses({VetControllerTests.class, VetRepositoryTest.class, CheckSettingsButton.class})
public class SuiteNR11 {

}
