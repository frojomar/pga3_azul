package org.springframework.samples.petclinic.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.samples.petclinic.selenium.GetVetName;
import org.springframework.samples.petclinic.selenium.InsertVet;
import org.springframework.samples.petclinic.vet.VetControllerTests;
import org.springframework.samples.petclinic.vet.VetRepositoryTest;

@RunWith(Suite.class)
@SuiteClasses({InsertVet.class, VetControllerTests.class, GetVetName.class, VetRepositoryTest.class})
public class SuiteNR12 {

}
