package org.springframework.samples.petclinic.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.samples.petclinic.selenium.VetInfo;
import org.springframework.samples.petclinic.selenium.VetListLink;
import org.springframework.samples.petclinic.vet.VetControllerTests;
import org.springframework.samples.petclinic.vet.VetRepositoryTest;

@RunWith(Suite.class)
@SuiteClasses({VetControllerTests.class, VetRepositoryTest.class, VetListLink.class, VetInfo.class})
public class SuiteNR2 {

}
