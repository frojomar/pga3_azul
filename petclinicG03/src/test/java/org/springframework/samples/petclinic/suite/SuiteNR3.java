package org.springframework.samples.petclinic.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.samples.petclinic.selenium.PetSpace;

@RunWith(Suite.class)
@SuiteClasses({ PetSpace.class })
public class SuiteNR3 {

}
