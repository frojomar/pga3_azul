package org.springframework.samples.petclinic.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.samples.petclinic.owner.OwnerControllerTests;
import org.springframework.samples.petclinic.owner.OwnerRepositoryTests;
import org.springframework.samples.petclinic.selenium.DeleteOwner;

@RunWith(Suite.class)
@SuiteClasses({ DeleteOwner.class, OwnerControllerTests.class, OwnerRepositoryTests.class })
public class SuiteNR4 {

}
