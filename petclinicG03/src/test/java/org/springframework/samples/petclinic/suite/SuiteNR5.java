package org.springframework.samples.petclinic.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.samples.petclinic.selenium.CheckHomeVisits;
import org.springframework.samples.petclinic.vet.VetTests;

@RunWith(Suite.class)
@SuiteClasses({ VetTests.class, CheckHomeVisits.class })
public class SuiteNR5 {

}
