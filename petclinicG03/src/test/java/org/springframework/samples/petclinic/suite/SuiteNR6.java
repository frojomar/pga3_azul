package org.springframework.samples.petclinic.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.samples.petclinic.owner.VisitControllerTests;
import org.springframework.samples.petclinic.selenium.EditVisit;
import org.springframework.samples.petclinic.selenium.EditVisitDetail;
import org.springframework.samples.petclinic.visit.VisitRepositoryTests;

@RunWith(Suite.class)
@SuiteClasses({VisitRepositoryTests.class/*, VisitControllerTests.class*/,
	EditVisit.class, EditVisitDetail.class})
public class SuiteNR6 {

}
