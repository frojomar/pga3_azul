package org.springframework.samples.petclinic.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.samples.petclinic.owner.PetControllerTests;
import org.springframework.samples.petclinic.owner.PetRepositoryTests;
import org.springframework.samples.petclinic.owner.PetTests;
import org.springframework.samples.petclinic.selenium.DeletePet;

@RunWith(Suite.class)
@SuiteClasses({ DeletePet.class, PetTests.class, PetControllerTests.class,
	PetRepositoryTests.class})
public class SuiteNR7 {

}
