package org.springframework.samples.petclinic.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.samples.petclinic.selenium.CheckWidth100;

@RunWith(Suite.class)
@SuiteClasses({CheckWidth100.class})
public class SuiteNR8 {

}
