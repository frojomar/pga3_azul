package org.springframework.samples.petclinic.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.samples.petclinic.owner.VisitControllerTests;
import org.springframework.samples.petclinic.selenium.AddVisit;
import org.springframework.samples.petclinic.vet.VetTests;
import org.springframework.samples.petclinic.visit.VisitTests;

@RunWith(Suite.class)
@SuiteClasses({AddVisit.class, VetTests.class, VisitTests.class/*, VisitControllerTests.class*/})
public class SuiteNR9 {

}
