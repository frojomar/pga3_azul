package org.springframework.samples.petclinic.vet;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

/**
 * Test class for the {@link VetController}
 */
@RunWith(SpringRunner.class)
@WebMvcTest(VetController.class)
public class VetControllerTests {
	
	private static final int TEST_VET_ID = 1;
	

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VetRepository vets;

    @Before    
    public void setup() {
        Vet james = new Vet();
        james.setFirstName("James");
        james.setLastName("Carter");
        james.setId(TEST_VET_ID);
        Vet helen = new Vet();
        helen.setFirstName("Helen");
        helen.setLastName("Leary");
        helen.setId(2);
        Specialty radiology = new Specialty();
        radiology.setId(1);
        radiology.setName("radiology");
        helen.addSpecialty(radiology);
        
        given(this.vets.findAll()).willReturn(Lists.newArrayList(james, helen));
        given(this.vets.findById(TEST_VET_ID)).willReturn(james);
    }

    @Test
    public void testShowVetListHtml() throws Exception {
        mockMvc.perform(get("/vets.html"))
            .andExpect(status().isOk())
            .andExpect(model().attributeExists("vets"))
            .andExpect(view().name("vets/vetList"));
    }

    @Test
    public void testShowResourcesVetList() throws Exception {
        ResultActions actions = mockMvc.perform(get("/vets")
            .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
        actions.andExpect(content().contentType("application/json;charset=UTF-8"))
            .andExpect(jsonPath("$.vetList[0].id").value(1));
    }
    
    @Test 
    public void testShowVet() throws Exception{
		mockMvc.perform(get("/vets/{vetId}", TEST_VET_ID))
		.andExpect(status().isOk())
        .andExpect(model().attribute("vet", hasProperty("firstName", is("James"))))
        .andExpect(model().attribute("vet", hasProperty("lastName", is("Carter"))))
		.andExpect(view().name("vets/vetDetails"));
    }

    //NR 11
    @Test
    public void testShowVetSpecialtiesListHtml() throws Exception {
        mockMvc.perform(get("/vets/vetSpecialties"))
            .andExpect(status().isOk())
            .andExpect(model().attributeExists("specialties"))
            .andExpect(view().name("vets/vetSpecialties"));
    }
    
    //FIN NR 11
    
	//NR 12
	@Test
	public void testInitCreationForm() throws Exception {
        mockMvc.perform(get("/vets/new"))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("vet"))
        .andExpect(view().name("vets/createOrUpdateVetForm"));
	}
	
	@Test
	public void testProcessCreationFormSuccess() throws Exception {
        mockMvc.perform(post("/vets/new")            
        		.param("firstName", "Joe")
                .param("lastName", "Bloggs")
            )
                .andExpect(status().is3xxRedirection());
	}

    @Test
    public void testProcessCreationFormHasErrors() throws Exception {
        mockMvc.perform(post("/vets/new")
            .param("firstName", "Joe")
        )
            .andExpect(status().isOk())
            .andExpect(view().name("vets/createOrUpdateVetForm"));
    }
	// FIN DE NR 12
    
}
