package org.springframework.samples.petclinic.visit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.PetType;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class VisitRepositoryTests {
	
	@Autowired
	private VisitRepository visits;
	
	private Visit visit;
	
	@Before
	public void init() {
		//Collection <Pet> cPets = 
		
		if (this.visit==null) {
			visit = new Visit ();
			
			Owner ownerJuan = new Owner();
			ownerJuan.setId(1);
			ownerJuan.setFirstName("Juan");
			ownerJuan.setLastName("Perez");
			ownerJuan.setAddress("Plaza de España");
			ownerJuan.setCity("Caceres");
			ownerJuan.setTelephone("111111111");
			
			PetType type= new PetType();
			type.setId(1);
			type.setName("dog");
			
			Pet pet= new Pet();
			pet.setBirthDate(LocalDate.parse("2008-05-10"));
			pet.setWeight((float)10.6);
			pet.setComments("It bites");
			pet.setName("Jack");
			pet.setType(type);
			

			ownerJuan.addPet(pet);

			Vet vet = new Vet();
			
			vet.setFirstName("Jose");
			vet.setHomeVisits(false);
			vet.setId(1);
			vet.setLastName("Rojo");
			
			visit.setVet(vet);
			visit.setDate(LocalDate.parse("2018-12-12"));
			visit.setDescription("Ala rota");
			visit.setPetId(pet.getId());
			visit.setId(1);
			
			pet.addVisit(visit);
			
			this.visits.save(visit);
		}
	}
	
	
	// NR6
	
	@Test
	public void testFindById() {
		Visit aVisit = this.visits.findById(visit.getId());
		
		assertNotNull(aVisit);
		
		assertEquals(aVisit.getDate(), visit.getDate());
		assertEquals(aVisit.getDescription(), visit.getDescription());
		assertEquals(aVisit.getId(), visit.getId());
		assertEquals(aVisit.getPetId(), visit.getPetId());

	}
	
	//FIN NR6
	
	
	@After
	public void finish() {
		this.visit=null;
	}
	

}