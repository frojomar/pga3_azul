package org.springframework.samples.petclinic.visit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import java.time.LocalDate;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.samples.petclinic.vet.Vet;


public class VisitTests {
	
	public static Visit visit;
	public static Visit visit2;
	public static Vet vet;
	
	@BeforeClass
	//This method is called before executing this suite
	public static void initClass() {
		vet= new Vet();
		
		visit = new Visit();
		visit.setPetId(1);
		visit.setDate(LocalDate.parse("2010-05-26"));
		visit.setDescription("Ala rota");
		visit.setVet(vet);
		visit.setId(1);
	}
	
	@Before
	//This test is executed before each test created in this suite
	public void init() {
	}
	
	@Test
	public void testVisit() {
		assertNotNull(visit);				
	}
	
	@Test
	public void testVisit2() {
		assertNull(visit2);				
	}
	
	//NR9
	@Test
	public void testVet() {
		assertNotNull(visit.getVet());
		assertEquals(visit.getVet(), vet);
		assertNotEquals(visit.getVet(), new Vet());		
	}
	//FIN NR9
	
    
    
    @AfterClass
    //This method is executed after all the tests included in this suite are completed.
    public static void finishClass() {
    	vet = null;
    	visit=null;
    }
    

}